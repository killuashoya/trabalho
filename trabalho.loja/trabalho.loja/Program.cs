﻿using System;
using System.Collections.Generic;
using trabalho.Diogo.ZeSousa;

namespace trabalho
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("◄ Bem Vindo ►");
            Console.WriteLine("Oque gostaria de pesquisar? \n (hoodie, t-shirt,calças.)");
            
            //________________________________________________________________

            #region hoodies
            List<hoodie> hoodies = new List<hoodie>();
            //_______________________
            hoodie carhartt = new hoodie("carhartt", "M", 80);
            hoodies.Add(carhartt);
            hoodie lacoste = new hoodie("lacoste", "S", 70);
            hoodies.Add(lacoste);
            hoodie tommy = new hoodie("tommy", "L", 50);
            hoodies.Add(tommy);
            hoodie champion = new hoodie("champion", "XL", 40);
            hoodies.Add(carhartt);
            hoodie ralph = new hoodie("ralph", "XS", 90);
            hoodies.Add(carhartt);
            #endregion

            //________________________________________________________________

            #region t-shirt
            List<t_shirt> tshirts = new List<t_shirt>();
            //_______________________
            t_shirt NY = new t_shirt("MLB New York Yankees", "L", 35);
            tshirts.Add(NY);
            t_shirt Champion = new t_shirt("champion basica", "M", 20);
            tshirts.Add(Champion);
            t_shirt Nike = new t_shirt("Nike basica", "XL", 50);
            tshirts.Add(Nike);
            t_shirt adidas = new t_shirt("Adidas desportiva", "M", 29);
            tshirts.Add(adidas);
            t_shirt calvin = new t_shirt("calvin klein basica", "XS", 90);
            tshirts.Add(calvin);
            #endregion

            //________________________________________________________________

            #region calças
            List<calça> calcas = new List<calça>();
            //_______________________
            calça tiffosi = new calça("ganga", "TIFFOSI basica", 48, 35);
            calcas.Add(tiffosi);
            calça Carhartt = new calça("ganga", "Carhartt WIP Simple", 44, 50);
            calcas.Add(Carhartt);
            calça springfield = new calça("ganga", "springfield basica", 48, 35);
            calcas.Add(springfield);
            calça Carhartt_J = new calça("cargo", "Carhartt Regular Cargo", 48, 40);
            calcas.Add(Carhartt_J);
            calça Lacoste = new calça("jogging", "calças jogging", 46, 100);
            calcas.Add(Lacoste);
            #endregion
            //________________________________________________________________
            #region carrinho

            List<string> CarrinhoCompras = new List<string>();

            #endregion
            //________________________________________________________________
            #region cash 

            int dinh = 250;

            #endregion
            //________________________________________________________________

            for (int i = 0; i < 5; i++)
            {
                #region hoodiesresp
                string resp1 = Console.ReadLine();
                if (resp1 == "hoodie")
                {
                    Console.WriteLine("Em loja temos " + carhartt.nome1 + ", " + lacoste.nome1 + ", " + tommy.nome1 + ", " + champion.nome1 + ", " + ralph.nome1);

                    Console.WriteLine("Gostaria de ver algum modelo em específico?");
                    string resphoodie = Console.ReadLine();
                    if (resphoodie == "carhartt")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + carhartt.tamanho1 + " e a custar " + carhartt.preco1);
                    }
                    if (resphoodie == "lacoste")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + lacoste.tamanho1 + " e a custar " + lacoste.preco1);
                    }
                    if (resphoodie == "tommy")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + tommy.tamanho1 + " e a custar " + tommy.preco1);
                    }
                    if (resphoodie == "champion")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + champion.tamanho1 + " e a custar " + champion.preco1);
                    }
                    if (resphoodie == "ralph")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + ralph.tamanho1 + " e a custar " + ralph.preco1);
                    }
                }
                #endregion
                //________________________________________________________________
                #region tshirt
                if (resp1 == "t-shirt")
                {
                    Console.WriteLine("Em loja temos " + NY.nome2 + ", " + Champion.nome2 + ", " + Nike.nome2 + ", " + adidas.nome2 + ", " + calvin.nome2);

                    Console.WriteLine("Gostaria de ver algum modelo em específico?");
                    string resptshirt = Console.ReadLine();

                    if (resptshirt == "NY")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + NY.tamanho2 + " e a custar " + NY.preco2);
                    }
                    if (resptshirt == "Champion")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + Champion.tamanho2 + " e a custar " + Champion.preco2);
                    }
                    if (resptshirt == "Nike")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + Nike.tamanho2 + " e a custar " + Nike.preco2);
                    }
                    if (resptshirt == "adidas")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + adidas.tamanho2 + " e a custar " + adidas.preco2);
                    }
                    if (resptshirt == "calvin")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + calvin.tamanho2 + " e a custar " + calvin.preco2);
                    }
                }
                #endregion
                //________________________________________________________________
                #region calcas 
                if (resp1 == "calça")
                {
                    Console.WriteLine("Em loja temos " + tiffosi.nome3 + ", " + Carhartt.nome3 + ", " + springfield.nome3 + ", " + Carhartt_J.nome3 + ", " + Lacoste.nome3);

                    Console.WriteLine("Gostaria de ver algum modelo em específico?");
                    string respcalcas = Console.ReadLine();

                    if (respcalcas == "tiffosi")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + tiffosi.tamanho3 + " e a custar " + tiffosi.preco3);
                    }
                    if (respcalcas == "Carhartt")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + Carhartt.tamanho3 + " e a custar " + Carhartt.preco3);
                    }
                    if (respcalcas == "springfield")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + springfield.tamanho3 + " e a custar " + springfield.preco3);
                    }
                    if (respcalcas == "Carhartt_J")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + Carhartt_J.tamanho3 + " e a custar " + Carhartt_J.preco3);
                    }
                    if (respcalcas == "Lacoste")
                    {
                        Console.WriteLine("Neste momento so temos um hoodie na loja com o tamanho " + Lacoste.tamanho3 + " e a custar " + Lacoste.preco3);
                    }

                }
                    #endregion
                //________________________________________________________________

                Console.WriteLine("deseja levar o produtos? \n (sim/nao)");
                string respos = Console.ReadLine();
                if (respos == "sim")
                {
                    Console.WriteLine("Insira o nome do produto");
                    string produto = Console.ReadLine();

                    #region comprar.hoodie
                    if (produto == "carhartt")
                    {
                        CarrinhoCompras.Add("carhartt");
                        Console.WriteLine("O total a pagar é " + carhartt.preco1);
                        dinh = dinh - carhartt.preco1;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    if (produto == "lacoste")
                    {
                        CarrinhoCompras.Add("lacoste");
                        Console.WriteLine("O total a pagar é " + lacoste.preco1);
                        dinh = dinh - lacoste.preco1;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    if (produto == "tommy")
                    {
                        CarrinhoCompras.Add("tommy");
                        Console.WriteLine("O total a pagar é " + tommy.preco1);
                        dinh = dinh - tommy.preco1;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    if (produto == "champion")
                    {
                        CarrinhoCompras.Add("champion");
                        Console.WriteLine("O total a pagar é " + champion.preco1);
                        dinh = dinh - champion.preco1;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    if (produto == "ralph")
                    {
                        CarrinhoCompras.Add("ralph");
                        Console.WriteLine("O total a pagar é " + ralph.preco1);
                        dinh = dinh - ralph.preco1;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    #endregion

                    #region comprar.t-shirt
                    if (produto == "NY")
                    {
                        CarrinhoCompras.Add("NY");
                        Console.WriteLine("O total a pagar é " + NY.preco2);
                        dinh = dinh - NY.preco2;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    if (produto == "Champion")
                    {
                        CarrinhoCompras.Add("Champion");
                        Console.WriteLine("O total a pagar é " + Champion.preco2);
                        dinh = dinh - Champion.preco2;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    if (produto == "Nike")
                    {
                        CarrinhoCompras.Add("Nike");
                        Console.WriteLine("O total a pagar é " + Nike.preco2);
                        dinh = dinh - Nike.preco2;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    if (produto == "adidas")
                    {
                        CarrinhoCompras.Add("adidas");
                        Console.WriteLine("O total a pagar é " + adidas.preco2);
                        dinh = dinh - adidas.preco2;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    if (produto == "calvin")
                    {
                        CarrinhoCompras.Add("calvin");
                        Console.WriteLine("O total a pagar é " + calvin.preco2);
                        dinh = dinh - calvin.preco2;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    #endregion

                    #region comprar.calças
                    if (produto == "tiffosi")
                    {
                        CarrinhoCompras.Add("tiffosi");
                        Console.WriteLine("O total a pagar é " + tiffosi.preco3);
                        dinh = dinh - tiffosi.preco3;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    if (produto == "Carhartt")
                    {
                        CarrinhoCompras.Add("Carhartt");
                        Console.WriteLine("O total a pagar é " + Carhartt.preco3);
                        dinh = dinh - Carhartt.preco3;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    if (produto == "springfield")
                    {
                        CarrinhoCompras.Add("springfield");
                        Console.WriteLine("O total a pagar é " + springfield.preco3);
                        dinh = dinh - springfield.preco3;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    if (produto == "Carhartt_J")
                    {
                        CarrinhoCompras.Add("Carhartt_J");
                        Console.WriteLine("O total a pagar é " + Carhartt_J.preco3);
                        dinh = dinh - Carhartt_J.preco3;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    if (produto == "Lacoste")
                    {
                        CarrinhoCompras.Add("Lacoste");
                        Console.WriteLine("O total a pagar é " + Lacoste.preco3);
                        dinh = dinh - Lacoste.preco3;
                        Console.WriteLine("OBRIGADO PELA COMPRA!!");
                    }
                    #endregion
                }
                if (respos == "nao")
                {
                    Console.ReadLine();
                }

                break;
            }

        }
    }
}
